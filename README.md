# Group 8 IE High Frequency Trading Technology Final Report

## Teammates
<b> Cristian Ocampo-Padilla </b>

* I am a Senior in Computer Science at the University of Illinois at Urbana-Champaign. I am graduating in May 2024, and have taken courses in artificial intelligence, data mining, and software engineering. I have experience working in industry as a Software and Cnotrols Intern at Navistar. My skills include Python(PyTorch, Pandas, and NumPy), C/C++, Java, JavaScript, Assembly, and Linux OS. I am on Linkedin as [Cristian Ocampo-Padilla](https://www.linkedin.com/in/cocampo-padilla/) and I can be reached by email at cocampopadilla@gmail.com.

## Introduction
